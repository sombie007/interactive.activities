**FOR AN OFFICIAL RELEASE PLEASE DOWNLOAD THE PACKAGE FROM [UiPath Connect!](https://go.uipath.com/component/notification-activities)**

UiPath Team Interactive Activities Package.

This package idea and original code is based on the [Attended Robot Status Window](https://go.uipath.com/component/attended-robot-status-window) by [Cornel Dumitrascu](https://connect.uipath.com/profile/cornel-dumitrascu).
This package provides several activities for displaying notifications about Process progress

1. **Display Message** - displays a messages with customizable text, background color, font color, and font size. The message is not stopping the execution of the current process. Returns a window handle ([HWND](https://stackoverflow.com/questions/1635645/what-is-hwnd-in-vc)) which could be used by other activities. Will close when the robot execution stops or by using **Close By Handle** activity. Could be configured to disappear on timed delay by providing a value for the *Hide After* argument. If an initialized window Handle is provided will update an existing window (so will act as **Update Message**). From version 1.4 you could use Presets to quickly change colors of the window.
2. **Update Message** - update previously displayed messagew with new text and/or colors. Requires a window handle which could be acquired from **Display Message** activity.
3. **Close By Handle** - closes a window by its handle (aka as [HWND](https://stackoverflow.com/questions/1635645/what-is-hwnd-in-vc)). Could be used to stop showing a Display Message window. Requires a window handle which could be acquired from **Display Message** activity.
4. **Windows Notification** - displays a Microsoft Windows Notification (also called 'Toast') with customizable title and text. You could configure the *Title* and the *Message* arguments. If the *Message* is not supplied it will be omitted from the notification. __Note__ that on some UiPath installations the notification may not be visible. This is usually due to unusual shortcut/location. Please contact the activity author for the version which could work on your system.
5. **Progress Bar** - displays a progress bar in the center of the screen. Values from *0* to *100*. Returns a Handle after the first call which could be used to update the bar value/color (just call **Progress Bar** with new values) or close it when it is not needed with **Close By Handle**. Color could be configured to be one of the predefined Windows colors (green, yellow and red).

Sample [UiPath Studio project](https://gitlab.com/uipathteam/interactive.activities/tree/master/InteractiveTest) demonstrates the capabilities for the activities.

![Screenshot](https://gitlab.com/uipathteam/interactive.activities/raw/master/UiPathTeam.Interactive.Activities/Resources/Screenshot3.png?inline=false)
