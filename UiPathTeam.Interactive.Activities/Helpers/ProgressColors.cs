﻿namespace UiPathTeam.Interactive.Activities.Properties
{
    public enum ProgressColors
    {
        [LocalizedDisplayName(nameof(Resources.None))]
        [LocalizedDescription(nameof(Resources.None))]
        None,
        [LocalizedDisplayName(nameof(Resources.Green))]
        [LocalizedDescription(nameof(Resources.Green))]
        Green,
        [LocalizedDisplayName(nameof(Resources.Red))]
        [LocalizedDescription(nameof(Resources.Red))]
        Red,
        [LocalizedDisplayName(nameof(Resources.Yellow))]
        [LocalizedDescription(nameof(Resources.Yellow))]
        Yellow
    }
}