﻿namespace UiPathTeam.Interactive.Activities.Properties
{
    public enum WindowSizes
    {
        [LocalizedDisplayName(nameof(Resources.Large))]
        [LocalizedDescription(nameof(Resources.Large))]
        Large,
        [LocalizedDisplayName(nameof(Resources.Normal))]
        [LocalizedDescription(nameof(Resources.Normal))]
        Normal,
        [LocalizedDisplayName(nameof(Resources.Small))]
        [LocalizedDescription(nameof(Resources.Small))]
        Small,
    }
}