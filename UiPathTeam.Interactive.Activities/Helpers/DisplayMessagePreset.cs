﻿namespace UiPathTeam.Interactive.Activities.Properties
{
    public enum DisplayMessagePresetEnum
    {
        [LocalizedDisplayName(nameof(Resources.Custom))]
        [LocalizedDescription(nameof(Resources.Custom))]
        Custom,
        [LocalizedDisplayName(nameof(Resources.Error))]
        [LocalizedDescription(nameof(Resources.Error))]
        Error,
        [LocalizedDisplayName(nameof(Resources.Warning))]
        [LocalizedDescription(nameof(Resources.Warning))]
        Warning,
        [LocalizedDisplayName(nameof(Resources.Success))]
        [LocalizedDescription(nameof(Resources.Success))]
        Success,
        [LocalizedDescription(nameof(Resources.Info))]
        [LocalizedDisplayName(nameof(Resources.Info))]
        Info,
        [LocalizedDescription(nameof(Resources.Dark))]
        [LocalizedDisplayName(nameof(Resources.Dark))]
        Dark,
    }

    public struct DisplayMessagePreset : System.IEquatable<DisplayMessagePreset>
    {
        public DisplayMessagePreset(string backColor = Constants.DEFAULT_BACK, string foreColor = Constants.DEFAULT_FORE, float fontSize = Constants.DEFAULT_FONTSIZE, string fontName = Constants.DEFAULT_FONTNAME) {
            BackColor = backColor;
            ForeColor = foreColor;
            FontSize = fontSize;
            FontName = fontName;
        }

        public string FontName { get; set; }
        public float FontSize { get; set; }
        public string ForeColor { get; set; }
        public string BackColor { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as DisplayMessagePreset?);
        }

        public bool Equals(DisplayMessagePreset obj)
        {
            return (obj.ForeColor == this.ForeColor && obj.BackColor == this.BackColor && obj.FontSize == this.FontSize && obj.FontName == this.FontName);
        }

        public override int GetHashCode()
        {
            return BackColor.GetHashCode() * ForeColor.GetHashCode() * FontSize.GetHashCode() * FontName.GetHashCode(); 
        }

        public static bool operator ==(DisplayMessagePreset left, DisplayMessagePreset right)
        {
            return left.Equals(right as DisplayMessagePreset?);
        }

        public static bool operator !=(DisplayMessagePreset left, DisplayMessagePreset right)
        {
            return !(left == right);
        }
    }   
}