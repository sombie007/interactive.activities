﻿using System;
using System.Activities;
using System.Collections.Generic;
using UiPathTeam.Interactive.Activities.Properties;
using System.Threading;
using UiPathTeam.Interactive.Activities.Forms;
using System.Windows.Markup;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.InformationPanel))]
    [LocalizedDescription(nameof(Resources.InformationPanelDescription))]
    public sealed class InfoPanel : NativeActivity
    {
        public Forms.InfoPanelForm ip = new Forms.InfoPanelForm();

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.TitleName))]
        [LocalizedDescription(nameof(Resources.TitleDescription))]
        public InArgument<string> Title { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.MessagesName))]
        [LocalizedDescription(nameof(Resources.MessagesDescription))]
        public InArgument<Dictionary<string,string>> Messages { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ValueName))]
        [LocalizedDescription(nameof(Resources.ValueDescription))]
        public InArgument<int> Value { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.HasButtonsName))]
        [LocalizedDescription(nameof(Resources.HasButtonsDescription))]
        [RequiredArgument]
        [OverloadGroup("Buttons")]        
        public InArgument<bool> HasButtons { get; set; } = true;

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ButtonPauseName))]
        [LocalizedDescription(nameof(Resources.ButtonPauseDescription))]
        [OverloadGroup("Buttons")]
        [DependsOn("HasButtons")]
        public InArgument<string> ButtonPause { get; set; } = Resources.ButtonPauseText;

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ButtonUnPauseName))]
        [LocalizedDescription(nameof(Resources.ButtonUnPauseDescription))]
        [OverloadGroup("Buttons")]
        [DependsOn("HasButtons")]
        public InArgument<string> ButtonUnPause { get; set; } = Resources.ButtonUnPauseText;

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ButtonCloseName))]
        [LocalizedDescription(nameof(Resources.ButtonCloseDescription))]
        [OverloadGroup("Buttons")]
        [DependsOn("HasButtons")]
        public InArgument<string> ButtonClose { get; set; } = Resources.ButtonCloseText;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.PositionName))]
        [LocalizedDescription(nameof(Resources.PositionDescription))]
        public WindowPositions Position { get; set; } = WindowPositions.BottomRight;

        [LocalizedCategory(nameof(Resources.Misc))]
        [LocalizedDisplayName(nameof(Resources.HandleName))]
        [LocalizedDescription(nameof(Resources.HandleDescription))]
        public InOutArgument<int> Handle { get; set; }

        [LocalizedCategory(nameof(Resources.Misc))]
        [LocalizedDisplayName(nameof(Resources.PauseName))]
        [LocalizedDescription(nameof(Resources.PauseDescription))]
        public InOutArgument<bool> Pause { get; set; }

        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.ResultName))]
        [LocalizedDescription(nameof(Resources.ResultDescription))]
        public OutArgument<string> Result { get; set; }

        private int? _handle;
        private bool _pause;

        protected override void Execute(NativeActivityContext context)
        {
            _handle = Handle.Get(context);
            ip.msgs = Messages.Get(context);
            ip.title = Title.Get(context);
            ip.value = Value.Get(context);
            ip.pauseText = ButtonPause.Get(context);
            ip.unpauseText = ButtonUnPause.Get(context);
            ip.closeText = ButtonClose.Get(context);
            ip.hasButtons = HasButtons.Get(context);

            _pause = Pause.Get(context);

            ip.pauseText = ButtonPause.Get(context);
            ip.unpauseText = ButtonUnPause.Get(context);

            //trying to connect to existing Window
            if (_handle.HasValue && _handle.Value != 0)
            {
                var closed = InfoPanelForm.getCloseStatus(_handle.Value);
                if (closed)
                {
                    Result.Set(context, "Closed");
                    return;
                }

                while (_pause)
                {
                    //Thread.Sleep(10);
                    System.Windows.Forms.Application.DoEvents();
                    _pause = InfoPanelForm.getPauseStatus(_handle.Value);
                }
                _pause = InfoPanelForm.getPauseStatus(_handle.Value);
                Forms.InfoPanelForm.UpdateWindow(_handle.Value, ip.title, (int)ip.value, ip.msgs, ip.pauseText, ip.unpauseText, ip.closeText, _pause);
                Pause.Set(context, _pause);
                Result.Set(context, _pause?"Paused":"OK");
                return;
            }

            _handle = ip.Handle.ToInt32();
            ip.initTable();
            ip.SetPosition(Position);
            ip.ShouldPause = _pause;
            ip.PauseRequested += Ip_PauseRequested;
            ip.FormClosed += Ip_FormClosed;

            Handle.Set(context, _handle);
            Pause.Set(context, _pause);

            Thread viewerThread = new Thread(delegate ()
            {
                var viewer = ip;
                viewer.Show();
                System.Windows.Threading.Dispatcher.Run();
            });

            viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            viewerThread.Start();

        }

        private void Ip_PauseRequested(object sender, EventArgs e)
        {
            _pause = InfoPanelForm.setPauseStatus(_handle.Value, (e as InfoPanelEventArgs).Pause);
        }

        private void Ip_FormClosed(object sender, EventArgs e)
        {
            Console.WriteLine("IP CLOSED");
            InfoPanelForm.setCloseStatus(_handle.Value, true);
            InfoPanelForm.setPauseStatus(_handle.Value, false);
        }

    }
}
