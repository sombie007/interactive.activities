﻿using System;
using System.Drawing;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Forms
{
    public partial class ProgressBarForm : GenericForm
    {
        public bool ShowValue { get; set; }
        public string Template { get; set; } = "{0}%";

        public ProgressBarForm()
        {
            InitializeComponent();
            _baseHeight = this.progressBar.Height;
            _baseWidth = this.progressBar.Width;
        }

        public void UpdateColor(ProgressColors color)
        {
            NativeMethods.SendMessage((int)progressBar.Handle, NativeMethods.PBM_SETBARCOLOR, (int)color, 0);
        }
        public static void UpdateColor(int handle, ProgressColors color)
        {
            NativeMethods.sendWindowsMessage(handle, NativeMethods.PBM_SETBARCOLOR, 0, (int)color);
        }
        public void UpdateValue(int value)
        {
            var prc = Math.Round((double)value / (progressBar.Maximum - progressBar.Minimum) * 100, 0);
            //value = 
            if (value == this.progressBar.Maximum) {
                ///HACK: for windows animation stupidity
                //this.progressBar.Maximum = 1000;
                //this.progressBar.Value = 1000;
                //this.progressBar.Value = 999;
                //this.progressBar.Value = 1000;
                //this.progressBar.Maximum = value;
            } else
            {
                //this.progressBar.Value = value;
            }

            if (this.progressBar.Style != ProgressBarStyle.Marquee)
            {
                if (progressBar.Value < value)
                {
                    for (var i = this.progressBar.Value; i <= value; i++) this.progressBar.PerformStep();
                } else
                {
                    this.progressBar.Value = value;
                }
            }
            if (ShowValue) this.labelValue.Show(); else this.labelValue.Hide();
            this.labelValue.Text = String.Format(this.Template, prc, value, progressBar.Minimum, progressBar.Maximum);
        }

        public static void UpdateValue(int handle, int value)
        {
            NativeMethods.sendWindowsMessage(handle, NativeMethods.WM_COPYDATA, NativeMethods.VALUE_UPDATE, value);
        }

        public void UpdateOpacity(double? opacity)
        {
            this.SetOpacity(opacity * 100);
        }

        public static void UpdateOpacity(int handle, double? opacity)
        {
            NativeMethods.sendWindowsStringMessage(handle, NativeMethods.OPACITY_UPDATE, opacity.Value.ToString());
        }

        public static void ScaleLabel(Label label, float stepSize = 0.5f)
        {
            //decrease font size if text is wider or higher than label
            while (lblTextSize() is Size s && s.Width > label.Width || s.Height > label.Height)
            {
                label.Font = new Font(label.Font.FontFamily, label.Font.Size - stepSize, label.Font.Style);
            }

            //increase font size if label width is bigger than text size
            while (label.Width > lblTextSize().Width)
            {
                var font = new Font(label.Font.FontFamily, label.Font.Size + stepSize, label.Font.Style);
                var nextSize = TextRenderer.MeasureText(label.Text, font);

                //dont make text width or hight bigger than label
                if (nextSize.Width > label.Width || nextSize.Height > label.Height)
                    break;

                label.Font = font;
            }

            Size lblTextSize() => TextRenderer.MeasureText(label.Text,
                new Font(label.Font.FontFamily, label.Font.Size, label.Font.Style));
        }

        public new void SetSize(WindowSizes? size)
        {
            base.SetSize(size);
            this.size = size ?? this.size;
            switch (this.size)
            {
                case WindowSizes.Small:
                    labelValue.Font = new Font(labelValue.Font.Name, 10F);
                    break;
                case WindowSizes.Large:
                    labelValue.Font = new Font(labelValue.Font.Name, 14F);
                    break;
                default:
                    labelValue.Font = new Font(labelValue.Font.Name, 12F);
                    break;
            }
            labelValue.Top = this.Height / 2 - labelValue.Height / 2;
            labelValue.Left = this.Width / 2 - labelValue.Width / 2;
        }

        protected override void WndProc(ref Message m)
        {
            try
            {
                switch (m.Msg)
                {
                    case NativeMethods.WM_COPYDATA:
                        switch ((int)m.WParam)
                        {
                            case NativeMethods.VALUE_UPDATE:
                                int value = m.LParam.ToInt32();
                                this.UpdateValue(value);
                                break;
                            case NativeMethods.OPACITY_UPDATE:
                                double opacity = m.LParam.ToInt32()/100;
                                this.UpdateOpacity(opacity);
                                break;
                        }
                        break;
                    case NativeMethods.PBM_SETBARCOLOR:
                        int color = m.LParam.ToInt32();
                        this.UpdateColor((ProgressColors)color);
                        break;
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            base.WndProc(ref m);
        }
    }
}
