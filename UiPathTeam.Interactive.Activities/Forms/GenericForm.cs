﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Forms
{
    public partial class GenericForm : Form
    {
        internal const int marginX = 50;
        internal const int marginY = 40;
        internal int _baseHeight, _baseWidth;
        public WindowPositions position = WindowPositions.Center;
        public WindowSizes size = WindowSizes.Normal;

        public GenericForm()
        {
            InitializeComponent();
            _baseHeight = this.Height;
            _baseWidth = this.Width;
        }

        public void CloseOnTimer(int milliseconds = 2000)
        {
            System.Threading.Thread.Sleep(milliseconds);
            this.Hide();
        }

        public static void InitForm()
        {

        }

        public void SetSize(WindowSizes? size)
        {
            this.size = size ?? this.size;
            double multW, multH;
            switch (this.size)
            {
                case WindowSizes.Small:
                    multW = 0.7;
                    multH = 0.7;
                    break;
                case WindowSizes.Large:
                    multW = 1.5;
                    multH = 1.5;
                    break;
                default:
                    multW = 1;
                    multH = 1;
                    break;
            }
            var updatedW = Math.Round(_baseWidth * multW, 0);
            var updatedH = Math.Round(_baseHeight * multH, 0);
            this.Width = (int)updatedW;
            this.Height = (int)updatedH;
        }

        public void SetPosition(WindowPositions? position)
        {
            Point location = this.Location;
            this.position = position ?? this.position;
            switch (this.position)
            {
                case WindowPositions.TopRight:
                    location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width - marginX, marginY);
                    break;
                case WindowPositions.BottomRight:
                    location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width - marginX, Screen.PrimaryScreen.WorkingArea.Height - this.Height - marginY);
                    break;
                case WindowPositions.TopLeft:
                    location = new Point(marginX, marginY);
                    break;
                case WindowPositions.BottomLeft:
                    location = new Point(marginX, Screen.PrimaryScreen.WorkingArea.Height - this.Height - marginY);
                    break;
                case WindowPositions.Center:
                    location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                    break;
                case WindowPositions.TopCenter:
                    location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, marginY);
                    break;
                case WindowPositions.BottomCenter:
                    location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height - this.Height - marginY);
                    break;
                default:
                    Console.WriteLine("Unknown window position received");
                    break;
            }
            //Console.WriteLine("Pos {0}:{1} -> {2} {3}", this.Location.X, this.Location.Y, location.X, location.Y);
            this.Location = location;
        }
        
        public void SetOpacity(double? opacity)
        {
            //Console.WriteLine(String.Format("opacity was: {0}, setting to {1}", frmStatus.Opacity, opacity));
            opacity = opacity == 0 ? Constants.TRANSPARENCY_OLD : opacity;
            opacity = opacity == 1 ? 0.99 : opacity; //to avoid fully transparent window at 1
            opacity = opacity ?? Constants.TRANSPARENCY_OLD;
            Opacity = opacity.Value;
        }        

        protected override void WndProc(ref Message m)
        {
            /*
    switch (m.Msg)
    {
        case NativeMethods.WM_COPYDATA:
            NativeMethods.COPYDATASTRUCT mystr = new NativeMethods.COPYDATASTRUCT();
            Type mytype = mystr.GetType();
            mystr = (NativeMethods.COPYDATASTRUCT)m.GetLParam(mytype);
            switch ((int)m.WParam)
            {
                case NativeMethods.WINDOW_HIDE:
                    if (this.IsHandleCreated)
                    {
                        this.Hide();
                    }
                    break;
                case NativeMethods.WINDOW_SHOW:
                    if (this.IsHandleCreated) {
                        this.Show();
                    }
                    break;
            break;
            }
            }*/

            base.WndProc(ref m);
        }

    }
}
