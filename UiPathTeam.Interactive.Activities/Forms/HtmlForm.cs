﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Forms
{
    public partial class HtmlForm : GenericForm
    {

        bool centered = false;

        public bool Centered { get => centered; set => centered = value; }

        public HtmlForm()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case NativeMethods.WM_COPYDATA:
                    NativeMethods.COPYDATASTRUCT mystr = new NativeMethods.COPYDATASTRUCT();
                    Type mytype = mystr.GetType();
                    mystr = (NativeMethods.COPYDATASTRUCT)m.GetLParam(mytype);

                    switch ((int)m.WParam)
                    {
                        case NativeMethods.WINDOW_UPDATE:
                            this.SetData(mystr.lpData);
                            break;
                        case NativeMethods.POSITION_UPDATE:
                            int pos_value;
                            _ = Int32.TryParse(mystr.lpData, out pos_value);
                            this.SetPosition((WindowPositions)pos_value);
                            break;
                    }
                    break;
            }

            base.WndProc(ref m);
        }

        static public void UpdateWindow(int handle, string title, string html, int width, int height)
        {
            var msgs = new Dictionary<string, string>();
            msgs["width"] = width.ToString();
            msgs["height"] = height.ToString();
            msgs["title"] = title;
            msgs["html"] = html;
            NativeMethods.sendWindowsStringMessage(handle, NativeMethods.WINDOW_UPDATE, JsonConvert.SerializeObject(msgs));
        }

        private bool SetData(string json)
        {
            try
            {
                var msgs = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                this.Name = msgs["title"];

                //That's what working to refresh the content of the webcontrol :(
                webBrowser1.Navigate("about:blank");
                webBrowser1.Document?.OpenNew(false);
                
                webBrowser1.Document?.Write(centered?CenterText(msgs["html"]):msgs["html"]);
                return true;
            }
            catch (Exception e)
            {
                //do nothing, it was not a valid JSON or an error with the control
                Console.WriteLine("Error updating HTML window: {0}, error: {1}", json, e.Message);
                return false;
            }
        }


        public void ShowHtml(string text)
        {
            webBrowser1.DocumentText = centered?CenterText(text):text;
        }

        internal void SetTitle(string title)
        {
            this.Text = title;
        }

        static public string CenterText(string html)
        {
//we need to support IE7 rendering for this, so most other solutions don't work
            string text = @"
<html><head><style>html, body {{ height: 100%; }}#middle {{*position: absolute;*top: 50%;*width: 100%;*text-align: center;}}#center {{*position: relative;*top: -50%;}}</style></head><body>
<div id='wrapper'><div id='middle'><div id='center'>{0}</div></div></div></body></html>";
            return string.Format(text, html);
        }

        internal void setScroll(bool scroll)
        {
            webBrowser1.ScrollBarsEnabled = scroll;
        }

        private void HtmlForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

    }
}