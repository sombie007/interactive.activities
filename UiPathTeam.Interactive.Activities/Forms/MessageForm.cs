﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Forms
{
    public class MessageForm : GenericForm
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        const int GWL_EXSTYLE = -20;
        const int WS_EX_LAYERED = 0x80000;
        const int WS_EX_TRANSPARENT = 0x20;

        public Label lblStatus;
        public bool DisplayStyle { get; set; } = true;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var style = GetWindowLong(this.Handle, GWL_EXSTYLE);
            SetWindowLong(this.Handle, GWL_EXSTYLE, style | WS_EX_LAYERED | WS_EX_TRANSPARENT);
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case NativeMethods.WM_COPYDATA:
                    NativeMethods.COPYDATASTRUCT mystr = new NativeMethods.COPYDATASTRUCT();
                    Type mytype = mystr.GetType();
                    mystr = (NativeMethods.COPYDATASTRUCT)m.GetLParam(mytype);

                    switch ((int)m.WParam)
                    {
                        case NativeMethods.TEXT_UPDATE:
                            SetData(mystr.lpData, null);
                            break;
                        case NativeMethods.COLOR_UPDATE:
                            string[] colors = mystr.lpData.Split('|');
                            if (colors.Count() > 1)
                            {
                                SetColorsFromString(colors[0], colors[1]);
                            }
                            break;
                        case NativeMethods.OPACITY_UPDATE:
                            double opacity;
                            var resOk = Double.TryParse(mystr.lpData, out opacity);
                            if (resOk) SetOpacity(opacity);
                            break;
                    }
                    break;
            }

            base.WndProc(ref m);
        }

        public void InitForm()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageForm));
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.BackColor = this.BackColor;
            //this.TransparencyKey = Color.Transparent;
            //this.TransparencyKey = this.BackColor;

            this.lblStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.AutoEllipsis = true;
            this.lblStatus.Text = "Text not set";
            this.lblStatus.AutoSize = true;
            
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(155, 17);
            this.lblStatus.Padding = new Padding(3, 3, 3, 3);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            //this.lblStatus.ForeColor = this.ForeColor;
            //this.lblStatus.BorderStyle = BorderStyle.Fixed3D;
            //this.lblStatus.BackColor = Color.FromName("Cyan");
            // 
            // MessageForm
            // 
            this.AccessibleDescription = "UiPath Message Form";
            this.AccessibleName = "UiPath Message Form";
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = false;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(533, 200);
            this.Controls.Add(this.lblStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MessageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

            //this.Paint += new System.Windows.Forms.PaintEventHandler(MessageForm_Paint);
        }

        public void SetFont(string fontName, float fontSize)
        {
            this.Font = new Font(fontName, fontSize, FontStyle.Regular, GraphicsUnit.Point);
        }

        public static void UpdateWindow(int handle, string fore = "", string back = "", double? opacity = null, string text = "")
        {
            if (!String.IsNullOrEmpty(fore) || !String.IsNullOrEmpty(back)) NativeMethods.sendWindowsStringMessage(handle, NativeMethods.COLOR_UPDATE, fore + "|" + back);
            if (!String.IsNullOrEmpty(text)) NativeMethods.sendWindowsStringMessage(handle, NativeMethods.TEXT_UPDATE, text);
            if (opacity.HasValue) NativeMethods.sendWindowsStringMessage(handle, NativeMethods.OPACITY_UPDATE, opacity.Value.ToString());
        }

        public void SetData(string message, WindowPositions? position)
        {
            lblStatus.Text = message;

            //Console.WriteLine(String.Format("label size {0}:{1} on {2}x{3}, pos {4}:{5}", lblStatus.Width, lblStatus.Height, this.ClientSize.Width, this.ClientSize.Height, lblStatus.Top, lblStatus.Left));

            if (this.DisplayStyle)
            {
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - marginX * 2;
                this.Height = lblStatus.Height;
                lblStatus.Left = this.Width/2 - lblStatus.Width/2;
            }
            else
            {
                this.Width = lblStatus.Width;
                this.Height = lblStatus.Height;
            }

            this.SetPosition(position);
        }

        public new void SetOpacity(double? opacity)
        {
            //Console.WriteLine(String.Format("opacity was: {0}, setting to {1}", frmStatus.Opacity, opacity));
            opacity = opacity == 0 ? (this.DisplayStyle ? Constants.TRANSPARENCY_NEW : Constants.TRANSPARENCY_OLD) : opacity;
            opacity = opacity == 1 ? 0.99 : opacity; //to avoid fully transparent window at 1
            opacity = opacity ?? (this.DisplayStyle ? Constants.TRANSPARENCY_NEW : Constants.TRANSPARENCY_OLD);
            Opacity = opacity.Value;
        }

        public void SetPosition (WindowPositions position)
        {
            Point location = this.Location;
            switch (this.position)
            {
                case WindowPositions.TopRight:
                    location = new Point(marginX, marginY);
                    break;
                case WindowPositions.BottomRight:
                    location = new Point(marginX, Screen.PrimaryScreen.WorkingArea.Height - this.Height - marginY);
                    break;
                case WindowPositions.TopLeft:
                    location = new Point(marginX, marginY);
                    break;
                case WindowPositions.BottomLeft:
                    location = new Point(marginX, Screen.PrimaryScreen.WorkingArea.Height - this.Height - marginY);
                    break;
                case WindowPositions.Center:
                    location = new Point(marginX, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                    break;
                default:
                    break;
            }
            this.Location = location;
        }

        public void SetColorsFromString(string fore, string back)
        {
            Color foreColor, backColor;
            int argb;
            
            if (!String.IsNullOrEmpty(fore))
            {
                var isint = Int32.TryParse(fore, out argb);
                foreColor = isint ? Color.FromArgb(argb) : Color.FromName(fore);
            }
            else foreColor = Color.Empty;

            if (!String.IsNullOrEmpty(back))
            {
                var isint = Int32.TryParse(back, out argb);
                backColor = isint?Color.FromArgb(argb):Color.FromName(back);
            } else backColor = Color.Empty;

            //empty color will mean that we should not update it
            this.SetColors(foreColor, backColor);
        }

        public void SetColors(Color foreColor, Color backColor)
        {
            if (foreColor != Color.Empty)
            {
                this.lblStatus.ForeColor = foreColor;
            }
            if (backColor != Color.Empty)
            {
                this.BackColor = backColor;
                lblStatus.BackColor = backColor;
            }
        }

        private void MessageForm_Paint(object sender, PaintEventArgs e)
        {
            //var hb = new HatchBrush(HatchStyle.Percent50, TransparencyKey);
            //e.Graphics.FillRectangle(hb, this.DisplayRectangle);
        }

        private void InitializeComponent()
        {
            InitForm();
        }
    }
}
 