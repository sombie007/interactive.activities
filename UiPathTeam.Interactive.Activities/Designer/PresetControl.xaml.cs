﻿using System.Windows.Controls;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Designer
{
    /// <summary>
    /// Interaction logic for PresetControl.xaml
    /// </summary>
    public partial class PresetControl : UserControl
    {
        public PresetControl()
        {
            InitializeComponent();
            preset.ItemsSource = LocalizedEnum<DisplayMessagePresetEnum>.GetLocalizedValues();
            preset.DisplayMemberPath = nameof(LocalizedEnum.Name);
            preset.SelectedValuePath = nameof(LocalizedEnum.Value);
        }
    }
}
