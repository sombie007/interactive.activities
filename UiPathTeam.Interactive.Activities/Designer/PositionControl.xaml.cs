﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Designer
{
    /// <summary>
    /// Interaction logic for PositonControl.xaml
    /// </summary>
    public partial class PositionControl : UserControl
    {
        public PositionControl()
        {
            InitializeComponent();
            position.ItemsSource = LocalizedEnum<WindowPositions>.GetLocalizedValues();
            position.DisplayMemberPath = nameof(LocalizedEnum.Name);
            position.SelectedValuePath = nameof(LocalizedEnum.Value);
        }
    }
}
