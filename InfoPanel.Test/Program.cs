﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities;
using UiPathTeam.Interactive.Activities.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace InfoPanel.Test
{
    static class Program
    {
    
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
           
            var text = "<b>bold test</b>, <i>italic test</i>, <span style=\"font-size:x-large\">css testcss testcss testcss <span style='background:red'>testcss</span> testcss <u>testcss</u> testcss testcss testcss</span>";
            var dh = new HtmlForm();
            text = HtmlForm.CenterText(text);

            dh.SetPosition(UiPathTeam.Interactive.Activities.Properties.WindowPositions.Center);
            dh.ShowHtml(text);
            dh.Show();

            var no = new DisplayToast();
            no.showTestToast("Super Message");

            var msg = new DisplayMessage();
            msg.Opacity = null;
            var handle = msg.ShowForm(true, null, new DisplayMessagePreset("Blue", "Green"), "Pirates Rule", WindowPositions.TopCenter);
            msg.Show();

            var pb = new ProgressBarForm();
            pb.position = UiPathTeam.Interactive.Activities.Properties.WindowPositions.TopRight;
            pb.SetPosition(pb.position);
            pb.progressBar.Value = 50;
            pb.Show();

            var ip = new InfoPanelForm();
            ip.PauseRequested += Ip_PauseRequested;

            ip.position = UiPathTeam.Interactive.Activities.Properties.WindowPositions.BottomRight;
            ip.value = 50;
            ip.msgs = new Dictionary<string, string>() {
                { "message1", "Test String" },
                { "message2", "longer test string test" },
                { "message3", "longer test string test" }
            };
            ip.initTable();
            InfoPanelForm.UpdateWindow((int)ip.Handle, "New title", 70, ip.msgs, "Pause Process", "Unpause Process", "Close", false);

            
            Application.Run(ip);
        }

        private static void Ip_PauseRequested(object sender, EventArgs e)
        {
            if ((e as InfoPanelEventArgs).Pause)
            {
                Console.WriteLine("PAUSE REQUESTED {0}", DateTime.Now);
            }
            else
            {
                Console.WriteLine("UNPAUSE REQUESTED {0}", DateTime.Now);
            }
        }
    }
}
